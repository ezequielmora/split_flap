﻿using UnityEngine;
using System.Collections;

public class SplitFlap : MonoBehaviour {

	public GameObject up;
	public GameObject down;
	public GameObject upFront;
	public GameObject downBack;

	public GameObject frontFlip;

	public Sprite[] upSprite;
	public Sprite[] downSprite;

	public Sprite blankSprite;

	public float angle;

	private int cardNumCurr = -1;
	private int cardNumPrev = 0;


	private bool valueFound;

	private int finalvalue;
	private int valueCount = -1;

	public bool checkValue;


    private AudioSource source;

    public AudioClip soundEffect;

	// Use this for initialization
	void Start () {


		for(int i = 0; i <= 9; i++)
		{
			upSprite[i] = Resources.Load<Sprite>((i)+"up");
			downSprite[i] = Resources.Load<Sprite>((i)+"down");
		}

		cardNumPrev = Random.Range(1,9);

		cardNumCurr = cardNumPrev-1;
		valueCount = cardNumCurr;
	}

	public void SetFlipper()
	{
		ResetFlaps();
		angle = 0;
		StartCoroutine("StartAnimation");
	}

	public void CheckFlipper(int value)
	{
		finalvalue = value;
		checkValue = true;
	}

	void ResetFlaps()
	{
		cardNumCurr++;
		cardNumPrev++;

		if(cardNumPrev > 9){
			cardNumPrev = 0;
			cardNumCurr = 9;
		}
		if(cardNumCurr > 9){
			cardNumCurr = cardNumPrev-1;
		}

		valueCount = cardNumCurr;

		up.GetComponent<SpriteRenderer>().sprite = upSprite[cardNumPrev];
		upFront.GetComponent<SpriteRenderer>().sprite = upSprite[cardNumCurr];
		down.GetComponent<SpriteRenderer>().sprite = downSprite[cardNumCurr];
		downBack.GetComponent<SpriteRenderer>().sprite = downSprite[cardNumPrev];
	}
	

	IEnumerator StartAnimation () {

        //Sonido
        source = gameObject.GetComponent<AudioSource>();

        while (!valueFound)
		{
            
            


            angle -=12;
			if(angle > -180)
			{
                
                frontFlip.transform.eulerAngles = new Vector3(angle,0,0);
				yield return null;
			}
			else
			{
                float pitch = Random.Range(0.9f, 1.1f);
                // Reproduccion
                source.pitch = pitch;
                source.PlayOneShot(soundEffect);
                //

                angle = -180;
				frontFlip.transform.eulerAngles = new Vector3(angle,0,0);

				angle = 0;
				frontFlip.transform.eulerAngles = new Vector3(angle,0,0);
				ResetFlaps();

				if(valueCount == finalvalue && checkValue)
				{
					valueFound = true;
					GameObject.Find("SplitFlipManager").GetComponent<SplitFlapManager>().AssignValue();
				}
			}
		}

		angle = 0;
		frontFlip.transform.eulerAngles = new Vector3(angle,0,0);

	}
}

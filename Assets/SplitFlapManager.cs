﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SplitFlapManager : MonoBehaviour {

	public List<SplitFlap> splitFlip = new List<SplitFlap>();

	public Text randomNumberText;

	private int count;

	private string splitString = "";

	public Vector3 incrementPos;
	private Vector3 tempincrementPos;


    //public AudioClip soundEffect;

    //private AudioSource source;


	void Start(){
		tempincrementPos = incrementPos;

        //source = this.gameObject.GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.Space))
		{


			Reset();

			int n = Random.Range(100,1000000000);
			randomNumberText.text = n.ToString();

			splitString = n.ToString();

			for(int i = 0; i < splitString.Length; i++){
				GameObject splitDial = Instantiate(Resources.Load("SplitFlap") as GameObject ,incrementPos,Quaternion.identity) as GameObject;
				incrementPos.x+=0.66f;
				splitDial.name = "SplitFlap"+i;
				splitFlip.Add(splitDial.GetComponent<SplitFlap>());

                

            }


            for (int i = 0; i < splitString.Length; i++)
			{
                if (i < splitFlip.Count)
                {
                    splitFlip[i].SetFlipper();
                    
                }
			}

			AssignValue();
		}
	}

	/// <summary>
	/// Assigns the value.
	/// if one slipflap completes its cycle than it calls this function so that 
	/// SplitManager can assign next splitflap its number
	/// </summary>
	public void AssignValue()
	{
		if(count < splitString.Length){

			if(count < splitFlip.Count){
				splitFlip[count].CheckFlipper(int.Parse(splitString[count].ToString()));
                
            }
			else{
				print ("count > splitFlip.Length");
                
            }
		}


		count++;

        if (count >= splitString.Length)
        {
            count = 0;
            
        }
	}

	void Reset()
	{
		for(int i = splitFlip.Count-1; i > -1; i--){
			Destroy(splitFlip[i].gameObject);
			splitFlip.RemoveAt(i);
			incrementPos = tempincrementPos;
			count = 0;
		}
	}
}
